package main

import (
	"flag"
	"gitlab.com/shadowy/go/sonar-scanner/model"
	"gitlab.com/shadowy/go/sonar-scanner/util"
	"os"
	"time"
)

var taskStatusForWait = []string{"IN_PROGRESS", "PENDING"}
var taskStatusForError = []string{"CANCELED", "FAILED"}

func main() {
	fileReport := flag.String("report", "./.scannerwork/report-task.txt", "sonar scanner report file")
	token := flag.String("token", "", "sonar token")
	flag.Parse()

	util.SetToken(*token)
	report, err := util.LoadIniFile(fileReport)
	if err != nil {
		os.Exit(-404)
		return
	}
	util.Console("info", "project         = %v", report["projectKey"])
	util.Console("info", "server-version  = %v", report["serverVersion"])
	var analysis *model.ResponseAnalysis
	for {
		analysis, err = model.ResponseAnalysis{}.Get(report["ceTaskUrl"])
		if err != nil {
			os.Exit(-500)
			return
		}
		if util.Find(taskStatusForWait, analysis.Status) < 0 {
			break
		}
		util.Console("notice", "analysis-status = %v", analysis.Status)
		time.Sleep(10 * time.Second)
	}

	color := "debug"
	if util.Find(taskStatusForError, analysis.Status) >= 0 {
		color = "error"
	}

	util.Console(color, "analysis-id     = %v", analysis.AnalysisId)
	util.Console(color, "analysis-status = %v", analysis.Status)
	util.Console(color, "analysis-time   = %v", analysis.ExecutionTime)
	if color == "error" {
		os.Exit(-500)
		return
	}
	result, err := model.ResponseProjectStatus{}.Get(report["serverUrl"], analysis.AnalysisId)
	if err != nil {
		os.Exit(-500)
		return
	}
	color = "debug"
	if result.Status != "OK" {
		color = "error"
	}
	util.Console(color, "project-status  = %v", result.Status)
	util.Console("notice", "---------------------------------------------------------------------------"+
		"------------")
	util.Console("notice", "|%-30s|%-10s|%10s|%10s|%10s|%10s|", "metric", "status",
		"value", "comparator", "warning", "error")
	util.Console("notice", "---------------------------------------------------------------------------"+
		"------------")
	for _, condition := range result.Conditions {
		color = "debug"
		if condition.Status != "OK" {
			color = "error"
		}
		util.Console(color, "|%-30s|%-10s|%10s|%10s|%10s|%10s|", condition.MetricKey, condition.Status,
			condition.ActualValue, condition.Comparator, condition.WarningThreshold, condition.ErrorThreshold)
	}
	util.Console("notice", "---------------------------------------------------------------------------"+
		"------------")
	if result.Status == "ERROR" {
		os.Exit(100)
	}
}
