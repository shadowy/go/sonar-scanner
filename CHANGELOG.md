# CHANGELOG

<!--- next entry here -->

## 0.2.5
2020-01-08

### Fixes

- fix issue with work with .net (f15842f5928d75619e8dacbcf6d6af49c6c8fb55)

## 0.2.4
2020-01-08

### Fixes

- fix sonar-dotnet-with-guard.sh (3309106a5530c61e4ad61674f09ff5eab90b6b96)

## 0.2.3
2020-01-08

### Fixes

- fix sonar-dotnet-with-guard.sh (210844fb8381cf5ca8ae40a14efdc2cab4b2135a)

## 0.2.2
2020-01-08

### Fixes

- fix build Dockerfile (81b5fcbc63d06b025fc0057c336b4c12d29de195)

## 0.2.1
2020-01-08

### Fixes

- install dotnet (68729b0e4b88c80cee59e492ff01de6bef5cf8fd)

## 0.2.0
2020-01-07

### Features

- add .net for sonar (5ac8bdd346878686e47eb1cc5fd5c9918c15a20b)

## 0.1.3
2020-01-06

### Fixes

- use node:lts-slim as base image (22ab8a090972dea7057d5f3d50da93f840858dd4)
- set error code if not success (fa7055baf06906d2f45e1f8653948378053cac43)

## 0.1.2
2020-01-06

### Fixes

- update Dockerfile (815a26bb0174c38c39b67e61288c6aa33ba4a33e)

## 0.1.1
2020-01-05

### Fixes

- fix Dockerfile (3402710db3f1cb6b8e6364af21a34262f722535b)

## 0.1.0
2020-01-05

### Features

- basic functionality (6e38159eeacfc32c3f6e86af8438fc5508230d68)
- update ci (4b2ca13d54b9a8814f8a6379c245be36e8c83ea8)

### Fixes

- update ci (9897e8f06defc2d6b6dfdc8dccc382c4e3b0da90)
- update ci (acc94044666aebcc50116a73fc4bbf2c5ae304b9)
- update ci (f8e5fcc96387830978341aa3f5397aee568a3f95)