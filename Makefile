version :=`git describe --abbrev=0 --tags $(git rev-list --tags --max-count=1)`

all: test lint-full

test:
	@echo ">> test"
	#@go test -coverprofile cover.out
	#@go tool cover -html=cover.out -o cover.html

lint-full: lint card

lint-badge-full: lint card card-badge

card:
	@echo ">> card"
	@goreportcard-cli -v

lint:
	@echo ">> lint"
	@golangci-lint run

card-badge:
	@echo ">>card-badge"
	@curl -X GET 'https://goreportcard.com/checks?repo=gitlab.com/shadowy/go/application-settings'

build: sonar-guard

swagger:
	@echo ">>swagger"
	@swag init -g ./cmd/nginx-docker-generator/main.go

sonar-guard:
	@echo ">>build: sonar-guard $(version)"
	@mkdir -p ./dist
	@env GO111MODULE=on CGO_ENABLED=0 GOOS=linux go build -mod=vendor -a -installsuffix cgo -o ./dist/$@ ./sonar-guard.go
	@chmod 777 ./dist/$@

docker:
	@echo ">>docker"
	docker build --file ./docker/Dockerfile . -t test-sonar

docker-release: docker-image

docker-login:
	@echo ">>docker-login"
	echo "${REGISTRY_PASSWORD}" | docker login -u "${REGISTRY_USER}" --password-stdin

docker-image:
	@echo ">>docker-image"
	docker build --file ./docker/Dockerfile . -t shadowy/sonar-scanner:$(version)
	docker push shadowy/sonar-scanner:$(version)

release: docker-login docker-release
