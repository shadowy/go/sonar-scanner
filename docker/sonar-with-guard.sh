#!/usr/bin/env bash
echo "sonar-with-guard"
sonar-scanner -Dsonar.host.url=$SONAR_SERVER -Dsonar.projectKey=$PROJECT_KEY -Dsonar.projectName=$PROJECT_NAME -Dsonar.projectVersion=$PROJECT_VERSION  -Dsonar.login=$SONAR_TOKEN
sonar-guard -token=$SONAR_TOKEN
