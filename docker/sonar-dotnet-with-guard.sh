#!/usr/bin/env bash
echo "sonar-dotnet-with-guard"
export DOTNET_ROOT=/opt/dotnet
export PATH="$PATH:/opt/dotnet:/root/.dotnet/tools"
echo "--------------------------------------------"
echo "restore"
echo "--------------------------------------------"
dotnet restore
echo "--------------------------------------------"
echo "begin"
echo "--------------------------------------------"
dotnet sonarscanner begin /k:$PROJECT_KEY  /d:sonar.host.url=$SONAR_SERVER /n:$PROJECT_NAME /v:$PROJECT_VERSION /d:sonar.login=$SONAR_TOKEN
echo "--------------------------------------------"
echo "build"
echo "--------------------------------------------"
dotnet build
echo "--------------------------------------------"
echo "end"
echo "--------------------------------------------"
dotnet sonarscanner end  /d:sonar.login=$SONAR_TOKEN
echo "--------------------------------------------"
echo "guard"
echo "--------------------------------------------"
sonar-guard -token=$SONAR_TOKEN -report=./.sonarqube/out/.sonar/report-task.txt
