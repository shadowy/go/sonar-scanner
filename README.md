# sonar-scanner

## Environment

| Environment variables | Description               |
|:----------------------|:--------------------------|
| $SONAR_SERVER         | url to sonar              |
| $SONAR_TOKEN          | token for access to token |
| $PROJECT_KEY          | project key               |
| $PROJECT_NAME         | project name              |
| $PROJECT_VERSION      | version of project        |
