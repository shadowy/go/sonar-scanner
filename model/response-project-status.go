package model

import (
	"encoding/json"
	"gitlab.com/shadowy/go/sonar-scanner/util"
)

type ResponseProjectStatus struct {
	Status     string `json:"status"`
	Conditions []struct {
		Status           string `json:"status"`
		MetricKey        string `json:"metricKey"`
		ActualValue      string `json:"actualValue"`
		Comparator       string `json:"comparator"`
		WarningThreshold string `json:"warningThreshold"`
		ErrorThreshold   string `json:"errorThreshold"`
	} `json:"conditions"`
}

type ResponseProject struct {
	Status ResponseProjectStatus `json:"projectStatus"`
}

func (model ResponseProjectStatus) Get(url string, analysisId string) (result *ResponseProjectStatus, err error) {
	body, err := util.HttpGet(url + "/api/qualitygates/project_status?analysisId=" + analysisId)
	if err != nil {
		return
	}
	var data ResponseProject
	err = json.Unmarshal(body, &data)
	if err != nil {
		util.Console("error", "[ERR] ResponseProjectStatus.Get parser: \"%v\"", err.Error())
		return
	}
	result = &data.Status
	return
}
