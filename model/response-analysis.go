package model

import (
	"encoding/json"
	"gitlab.com/shadowy/go/sonar-scanner/util"
)

type ResponseAnalysis struct {
	Id            string `json:"id"`
	AnalysisId    string `json:"analysisId"`
	Status        string `json:"status"`
	ExecutionTime int    `json:"executionTimeMs"`
}

type ResponseAnalysisTask struct {
	Task ResponseAnalysis `json:"task"`
}

func (model ResponseAnalysis) Get(url string) (result *ResponseAnalysis, err error) {
	body, err := util.HttpGet(url)
	if err != nil {
		return
	}
	var data ResponseAnalysisTask
	err = json.Unmarshal(body, &data)
	if err != nil {
		util.Console("error", "[ERR] ResponseAnalysis.Get parser: \"%v\"", err.Error())
		return
	}
	result = &data.Task
	return
}
