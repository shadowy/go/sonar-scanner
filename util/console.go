package util

import "fmt"

const (
	InfoColor    = "\033[1;34m%s\033[0m"
	NoticeColor  = "\033[1;36m%s\033[0m"
	WarningColor = "\033[1;33m%s\033[0m"
	ErrorColor   = "\033[38;5;31m%s\033[39;49m"
	DebugColor   = "\033[38;5;32m%s\033[39;49m"
)

func Console(color string, format string, params ...interface{}) {
	result := fmt.Sprintf(getColor(color), fmt.Sprintf(format, params...))
	fmt.Println(result)
}

func getColor(color string) string {
	switch color {
	case "info":
		return InfoColor
	case "notice":
		return NoticeColor
	case "warning":
		return WarningColor
	case "error":
		return ErrorColor
	case "debug":
		return DebugColor
	default:
		return DebugColor
	}
}
