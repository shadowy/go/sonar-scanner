package util

func Find(list []string, data string) int {
	for index, item := range list {
		if item == data {
			return index
		}
	}
	return -1
}
