package util

import (
	"io/ioutil"
	"strings"
)

func LoadIniFile(file *string) (result map[string]string, err error) {
	data, err := ioutil.ReadFile(*file)
	if err != nil {
		Console("error", "Report file \"%v\" not found\n", *file)
		return
	}
	result = make(map[string]string)
	lines := strings.Split(string(data), "\n")
	for _, line := range lines {
		pos := strings.Index(line, "=")
		if pos >= 0 {
			key := line[0:pos]
			value := line[pos+1:]
			result[key] = value
		}
	}
	return
}
