package util

import (
	"io/ioutil"
	"net/http"
)

var token string

func SetToken(tkn string) {
	token = tkn
}

func HttpGet(url string) (result []byte, err error) {
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		Console("error", "HttpGet create: \"%v\"", err.Error())
		return
	}
	if token != "" {
		request.SetBasicAuth(token, "")
	}
	client := &http.Client{}
	resp, err := client.Do(request)
	if err != nil {
		Console("error", "HttpGet do: \"%v\"", err.Error())
		return
	}
	defer func() { _ = resp.Body.Close() }()
	result, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		Console("error", "HttpGet body: \"%v\"", err.Error())
		return
	}
	return
}
